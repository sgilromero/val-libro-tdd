
public class Libro {
	private String titulo;
	private String autor;
	private int ejemplares;
	private int prestados;
	
	public String getTitulo() {
	    return titulo;
	}

	  // Setter
	public void setTitulo(String titulo) {
	    this.titulo = titulo;
	}
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getEjemplares() {
		return ejemplares;
	}

	public void setEjemplares(int ejemplares) {
		if(ejemplares >= 0) {
			this.ejemplares = ejemplares;
		}
		else {
			this.ejemplares = 0;
		}		
	}

	public int getPrestados() {
		return prestados;
	}

	public void setPrestados(int prestados) {
		this.prestados = prestados;
	}
	
	public Libro() {
		this.prestados = 0;
	}
	
	public Libro(String titulo, String autor, int ejemplares) {
		this.titulo = titulo;
		this.setAutor(autor);
		this.setEjemplares(ejemplares);
		this.setPrestados(0);
	}
	
	public boolean prestamo() throws Exception {
		if(this.getEjemplares() > this.getPrestados()) {
			this.setPrestados(this.getPrestados() + 1);
			
			return true;
		}
		else {
			throw new Exception("Exception");
		}
	}
	
	public boolean devolucion() throws Exception {
		if(this.getPrestados() > 0) {
			this.setPrestados(this.getPrestados() - 1);
			
			return true;
		}
		else {
			throw new Exception("Exception");
		}
	}	
}
