import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LibroTest {

	@Test
	public void testLibro1() throws Exception {
		Libro libro1 = new Libro("0.000 Leguas de Viaje Submarino", "Julio Verne", 1);
		
		assertEquals(libro1.getTitulo(), "0.000 Leguas de Viaje Submarino");
		assertEquals(libro1.getAutor(), "Julio Verne");
		assertEquals(libro1.getEjemplares(), 1);
		assertEquals(libro1.getPrestados(), 0);
	}
	
	@Test
	public void testLibro2() throws Exception {
		Libro libro2 = new Libro();
		libro2.setTitulo("Hamlet");
		libro2.setAutor("William Shakespeare");
		libro2.setEjemplares(2);
		
		assertEquals(libro2.getTitulo(), "Hamlet");
		assertEquals(libro2.getAutor(), "William Shakespeare");
		assertEquals(libro2.getEjemplares(), 2);
		assertEquals(libro2.getPrestados(), 0);
	}
	
	@Test
	public void testLibro3() throws Exception {
		Libro libro3 = new Libro("Don Quijote de la Mancha", "Miguel de Cervantes Saavedra", -4);
		
		assertEquals(libro3.getTitulo(), "Don Quijote de la Mancha");
		assertEquals(libro3.getAutor(), "Miguel de Cervantes Saavedra");
		assertEquals(libro3.getEjemplares(), 0);
		assertEquals(libro3.getPrestados(), 0);
	}

	@Test
	public void testLibro4() throws Exception {
		Libro libro1 = new Libro("0.000 Leguas de Viaje Submarino", "Julio Verne", 1);
		Libro libro2 = new Libro();
		libro2.setTitulo("Hamlet");
		libro2.setAutor("William Shakespeare");
		libro2.setEjemplares(2);
		Libro libro3 = new Libro("Don Quijote de la Mancha", "Miguel de Cervantes Saavedra", -4);
		
		//Libro 1 - 1 Ejemplar
		//Libro 2 - 2 Ejemplares
		//Libro 3 - 0 Ejemplar
		
		libro1.prestamo(); // -> Prestados 1
		assertEquals(libro1.getPrestados(), 1);		
		
		libro2.prestamo(); // -> Prestados 1
		assertEquals(libro2.getPrestados(), 1);	
		
		libro1.devolucion(); // -> Prestados 0
		assertEquals(libro1.getPrestados(), 0);
		
		Assertions.assertThrows(Exception.class, () -> libro3.prestamo()); // -> Prestados 0
		
		libro2.prestamo(); // -> Prestados 2
		assertEquals(libro2.getPrestados(), 2);
		
		Assertions.assertThrows(Exception.class, () -> libro2.prestamo()); // -> Prestados 2
		
		Assertions.assertThrows(Exception.class, () -> libro3.devolucion()); // -> Prestados 0
	}
}
