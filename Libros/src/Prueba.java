
public class Prueba {

	public static void main(String[] args) throws Exception {
		Libro libro1 = new Libro("0.000 Leguas de Viaje Submarino", "Julio Verne", 1);
		
		Libro libro2 = new Libro();
		libro2.setTitulo("Hamlet");
		libro2.setAutor("William Shakespeare");
		libro2.setEjemplares(2);

		Libro libro3 = new Libro("Don Quijote de la Mancha", "Miguel de Cervantes Saavedra", -4);
		
		libro1.prestamo();
		libro2.prestamo();
		libro1.devolucion();
		libro3.prestamo();
		libro2.prestamo();
		libro2.prestamo();
		libro3.devolucion();
		libro3.prestamo();
		
		System.out.println("Libro 1:");
		System.out.println("T�tulo: " + libro1.getTitulo());
		System.out.println("Autor: " + libro1.getAutor());
		System.out.println("Ejemplares: " + libro1.getEjemplares());
		System.out.println("Prestados: " + libro1.getPrestados());
		System.out.println();
		
		System.out.println("Libro 2:");
		System.out.println("T�tulo: " + libro2.getTitulo());
		System.out.println("Autor: " + libro2.getAutor());
		System.out.println("Ejemplares: " + libro2.getEjemplares());
		System.out.println("Prestados: " + libro2.getPrestados());
		System.out.println();
		
		System.out.println("Libro 3:");
		System.out.println("T�tulo: " + libro3.getTitulo());
		System.out.println("Autor: " + libro3.getAutor());
		System.out.println("Ejemplares: " + libro3.getEjemplares());
		System.out.println("Prestados: " + libro3.getPrestados());
		System.out.println();
	}
}
